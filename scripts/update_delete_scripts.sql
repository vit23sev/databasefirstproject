update nba_league.player

set age = 40
where first_name = 'LeBron' and second_name = 'James';


update nba_league.player

set is_captain = true
where first_name = 'Draymond' and second_name = 'Green';

update nba_league.player

set is_captain = false
where first_name = 'Stephen' and second_name = 'Curry';

delete
FROM nba_league.coach
WHERE first_name = 'Darvin' and last_name = 'Ham';

delete
from nba_league.player
where first_name = 'Russell' and second_name = 'Westbrook';
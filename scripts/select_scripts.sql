
--Игроки старше 30 лет
select *
from nba_league.player
where age > 30;

--Средний рост игрока по позиции
select position, round(avg(height), 1) as avg_height
from nba_league.player
group by position;

--Клубы, тренера которых раньше приводили команды к чемпионству
select name
from nba_league.basketball_club as outer_t
where (select count_of_wins from nba_league.coach as inner_t where outer_t.basketball_club_id = inner_t.basketball_club_id) > 0;

--Клубы по убыванию средней зарплаты у игроков
with avg_salaries as (select basketball_club_id, round(avg(salary)::numeric, 1) as avg_salary from nba_league.player group by basketball_club_id)
select name, avg_salary
from nba_league.basketball_club as main_table
join avg_salaries on main_table.basketball_club_id = avg_salaries.basketball_club_id
order by avg_salary desc;

--Тренеры, которые выиграли матчей больше, чем проиграли в текущем сезоне.
with win_clubs as (select basketball_club_id, current_count_of_wins
                   from nba_league.regular_championat
                   where current_count_of_wins > regular_championat.current_count_of_loses),
joined as (select win_clubs.basketball_club_id, current_count_of_wins, name
                from nba_league.basketball_club as t join win_clubs on win_clubs.basketball_club_id = t.basketball_club_id)
select first_name, last_name, name, current_count_of_wins
from nba_league.coach as main_table
right join joined on main_table.basketball_club_id = joined.basketball_club_id;


select first_name, second_name, position, salary, avg(salary) over w as avg_salary
from nba_league.player
window w as (partition by position);

select dense_rank() over w as rank, first_name, second_name, salary, basketball_club_id
from nba_league.player
window w as (partition by basketball_club_id
             order by salary desc);

select ntile(3) over w as tile, first_name, second_name, salary
from nba_league.player
window w as (order by salary desc);

with temp as (select first_name, second_name, salary, lag(salary, 1) over w as previous
from nba_league.player
window w as (order by salary))
select first_name, second_name, salary, previous, round((salary - previous) * 100.0 /  previous) as diff
from temp
window w as (order by salary desc);

select first_name, second_name, position, age, avg(age) over w as avg_age
from nba_league.player
window w as (partition by position);
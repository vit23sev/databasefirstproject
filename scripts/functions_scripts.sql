create function win_championship(club varchar)
returns numeric as $$
    update nba_league.basketball_club
    set count_of_titles = count_of_titles + 1
    where name = win_championship.club;
    select count_of_titles
    from nba_league.basketball_club
    where name = win_championship.club;
    $$ language sql;

select win_championship('Celtics') as answer;

create function summary_salary(club varchar)
returns numeric as $$
    select sum(salary)
    from nba_league.player t1
    join nba_league.basketball_club t2 on (t1.basketball_club_id = t2.basketball_club_id)
    where name = summary_salary.club;
    $$ language sql;

select summary_salary('Thunder') as answer;


create function max_height_in_position(pos varchar)
returns numeric as $$
select max(height)
from nba_league.player
where position = max_height_in_position.pos;
$$ language sql;

select max_height_in_position('point guard') as answer;

create type func_result as (name varchar, surname varchar);
create function increase_captain_salary(club_id int, value int)
returns func_result
as $$
    update nba_league.player
    set salary = salary + value
    where basketball_club_id = increase_captain_salary.club_id and player.is_captain = true;
    select first_name, second_name
    from nba_league.player
    where basketball_club_id = increase_captain_salary.club_id and player.is_captain = true;
    $$ language sql;

select increase_captain_salary(3, 10) as answer;
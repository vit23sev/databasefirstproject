CREATE SCHEMA NBA_League;
CREATE TABLE NBA_league.basketball_club (
    basketball_club_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(50),
    count_of_titles INTEGER,
    foundation_year INTEGER NOT NULL

);
CREATE TABLE NBA_league.game (
    game_id SERIAL PRIMARY KEY,
    date DATE NOT NULL,
    win_club_id SERIAL NOT NULL,
    lose_club_id SERIAL NOT NULL,
    score VARCHAR(50) NOT NULL,
    FOREIGN KEY (win_club_id) references NBA_league.basketball_club (basketball_club_id)
);

CREATE TABLE NBA_league.player (
    player_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    second_name VARCHAR(50) NOT NULL,
    age INTEGER NOT NULL,
    basketball_club_id SERIAL NOT NULL,
    height INTEGER NOT NULL,
    position VARCHAR(50) NOT NULL,
    salary FLOAT NOT NULL,
    is_captain BOOLEAN,
    FOREIGN KEY (basketball_club_id)
        REFERENCES NBA_league.basketball_club (basketball_club_id)
);

CREATE TABLE NBA_league.regular_championat (
    position SERIAL PRIMARY KEY,
    basketball_club_id SERIAL NOT NULL,
    current_count_of_wins INTEGER,
    current_count_of_loses INTEGER,
    FOREIGN KEY (basketball_club_id)
        REFERENCES NBA_league.basketball_club (basketball_club_id)
);

CREATE TABLE NBA_league.coach (
    coach_id SERIAL PRIMARY KEY,
    basketball_club_id SERIAL NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    career_start_year INTEGER NOT NULL,
    count_of_wins INTEGER NOT NULL,
    FOREIGN KEY (basketball_club_id)
        REFERENCES NBA_league.basketball_club (basketball_club_id)
);
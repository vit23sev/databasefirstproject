create view centers as
select *
from nba_league.player
where position = 'center';

create view coach_works_at as
select first_name, last_name, name, location
from nba_league.coach t1
join nba_league.basketball_club t2 on (t1.basketball_club_id = t2.basketball_club_id);

create view player_plays_at as
select first_name, second_name, name, location
from nba_league.player t1
join nba_league.basketball_club t2 on (t1.basketball_club_id = t2.basketball_club_id)
where is_captain = true;

create view lakers_wins as
select win_club, name as lose_club, date, score
from (select name as win_club, date, score, lose_club_id
from nba_league.basketball_club t1
join nba_league.game t2 on (t2.win_club_id = t1.basketball_club_id) where name = 'Lakers') as t3
join nba_league.basketball_club t4 on (t3.lose_club_id = t4.basketball_club_id);